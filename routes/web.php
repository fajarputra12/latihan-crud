<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/form','AuthController@bio'); 

Route::post('/welcome','AuthController@kirim');

Route::get('/data-table', function(){
    return view ('table.data-table');
});

Route::get('/master', function(){
    return view ('layout.master');
});
//CRUD Genre
//Route::get('/genre/create','GenreController@create');
Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');
Route::get('/cast','CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cats_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}','CastController@update');  
Route::delete('/cast/{cast_id}','CastController@destroy');  