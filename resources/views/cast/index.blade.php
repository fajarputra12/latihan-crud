@extends('layout.master')

@section('judul')
Isi Formulir dibawah ini
    
@endsection

@section('content')

<a href="/cast/create" class="btn btn-succes mb-3">Tambah Data</a>

<table class="table table-dark">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
      <tr>
        <td>{{$key +1 }}</td>
        <td>{{$item->nama }}</td>
        <td>{{$item->umur }}</td>
        <td>{{$item->bio }}</td>
        <td> 
            <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="#" class="btn btn-warning btn-sm">Edit</a>
            <form action="/cast/{{$item->id}}" method="POST">
                @method('delete')
                @csrf
            <a href="#" class="btn btn-danger btn-sm">Delete</a>
        </td>
      </tr>     
      @empty
      <tr>
        <td>Data Masih Kosong</td>
      </tr>  
      @endforelse
    </tbody>
  </table>   
@endsection
